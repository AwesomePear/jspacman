/*eslint-env browser*/
var doc = window.document;
var canvas = doc.getElementById("myCanvas");
var context = canvas.getContext("2d");

var demo_canvas = doc.getElementById("StartCanvas");
var demo_context = demo_canvas.getContext("2d");

const speed = 30;

var Foods = [];
var BigFoods = [];
var Caspers = [];
var Walls = [];
var DrawObjects = [];
var MoveObjects = [];
var Points = 0;
var GlobalScore = doc.getElementById("Score");
var GlobalTime = 0;
var GlobalTimeWatch = doc.getElementById("Timer");
var Lives = doc.getElementById("Lives");
var Button = doc.getElementById("Button");
var Bonus = doc.getElementById("BonusTimer");
var ScoreTable = doc.getElementById("ScoreTable");
var RememberBlock = doc.getElementById("RememberBlock");
var NameInput = doc.getElementById("Name");

const TimerWait = 1000;
const BonusTime = 10000;

const up = "up";
const down = "down";
const left = "left";
const right = "right";

const cellsize = 24;
const miscarriage = cellsize/12; //отступ от стенки

const StartLives = 3;

const Invulnerable = 3000;
const CadresHideWhenDead = 5;


const CasperRotationProbability = 0.9;
const BigFood = "b";
const SmallFood = "s";
const BigFoodSize = 5;
const SmallFoodSize = 2;
const BigFoodCost = 10;
const SmallFoodCost = 1;
const FoodColor = "#7a7a7a";
const CasperCost = 200;
const GrowUpTimer = 10000;
const HTMLlives = "<image src = 'pacman//pac6.png' width= '30' height='30'>";

const FORWARD = "F";
const BACKWARD = "B";
 
var Scores = [];

class ScoreLine{
    constructor(name,score){
        this.name = name;
        this.score = score;
    }
}

function ClearTable(){
    ScoreTable.innerHTML='<div class = "ScoreTableTitle" onclick="ShowHideScores();">Score table</div>';
}

function AddScoreLine(number,name,score){
    ScoreTable.innerHTML+='<div class="ScoreTableNumber">'+number+'</div><div class = "ScoreTableName">'+name+'</div><div class = "ScoreTableScore">'+score+'</div>'
}

function UpdateTable(){
    ClearTable();
    let i = 0;
    for (; i<Scores.length; i++)
        AddScoreLine(i+1,Scores[i].name,Scores[i].score);
    for (; i<10; i++)
        AddScoreLine(i+1,"(пусто)","-");
}

function AddScore(scoreline){
    for (let i = 0; i<Scores.length; i++)
        if (Scores[i].name.trim()==scoreline.name.trim())
            {
            if (Scores[i].score<scoreline.score){
                Scores[i].score = scoreline.score;
                Scores.sort((a, b) => a.score < b.score ? 1 : -1);
            }
            return;
        } 
    Scores.push(scoreline);
    Scores.sort((a, b) => a.score < b.score ? 1 : -1);
    if (Scores.length>10) Scores.pop();
}

function ShowHideScores(){
    if (ScoresHide) {
        UpdateTable();
        ScoresHide = false;
    } else {
        ClearTable();
        ScoresHide = true;
    }
}


function LoadCookies(){
    let all = document.cookie;
    let pairs = all.split(";");
    
    console.log(pairs);
    for (let i = 0; i<pairs.length; i++){
        let pair = pairs[i].split("=");
        AddScore(new ScoreLine(pair[0],parseInt(pair[1])));
    }
    
}

function CanBeRemembered(){
    return Points>Scores[Scores.length-1].score;
}

function Remember(){
    
    let name = NameInput.value;
    if (name=="") return;
    DeleteCookie(Scores[Scores.length-1].name);
    AddScore(new ScoreLine(name,Points));
    Points = 0;
    GlobalTime = 0;
    UpdateTable();
    SaveCookies();
    RememberBlock.style.visibility = "hidden";
}

function SaveCookies(){
    for (let i = 0; i<Scores.length; i++)
        AddCookie(Scores[i].name,Scores[i].score);
}


LoadCookies();
UpdateTable();
var ScoresHide = false;
ShowHideScores();

console.log(Scores);

function DeleteCookie(name){
    document.cookie = name+"=;max-age=-1";
}
function AddCookie(name,value){
    document.cookie = name+"="+value;
}


class myObject {
    constructor (x,y, size, color){
        this.x = x; 
        this.y = y; 
        this.size = size; 
        this.color = color;
        DrawObjects.push(this);
    }
    set(x,y){ 
        this.x = x; 
        this.y = y; 
    }
    draw(context){
        context.beginPath();
        context.arc(this.x, this.y, this.size, 0, Math.PI*2, false);
        context.fillStyle = this.color==undefined ? "#c0ff00" : this.color;
        context.fill();
        context.strokeStyle = "rgba(0, 0, 255, 1)";
        context.stroke();
        context.closePath();
    }
    
    collision(obj){
        return Distance(this.x,this.y,obj.x,obj.y)<=(this.size+obj.size);
    }
} 

function Distance(x1,y1,x2,y2){
    return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}

class Wall  {
     constructor (x,y, w, h){
        this.x = x; 
        this.y = y; 
        this.w = w; 
        this.h = h; 
        DrawObjects.push(this);
        Walls.push(this);
     }
    draw(context){
        context.beginPath();
        context.rect( this.x, this.y, this.w, this.h);
        context.fillStyle = "#2a377b";
        context.fill();
        context.closePath();  
    }
    contains(x,y){
        if (x>this.x && 
            x<this.x+this.w &&
            y>this.y &&
            y<this.y+this.h
           )
            return true;
        else return false;
        
    }
}

class Food extends myObject{
    constructor (x,y, type){
        super(x,y, (type == BigFood ? BigFoodSize : SmallFoodSize), FoodColor);
        this.type = type;
        Foods.push(this);
        if (this.type == BigFood) BigFoods.push(this);
    }
    
   delete(){
       for (var i = 0; i < Foods.length; i++)
            if (Foods[i] === this) {
                Foods.splice(i,1);
            }
       for (var i = 0; i < DrawObjects.length; i++)
            if (DrawObjects[i] === this) {
                DrawObjects.splice(i,1);
            }
       if (this.type == BigFood) 
           for (var i = 0; i < BigFoods.length; i++)
                if (BigFoods[i] === this) {
                     BigFoods.splice(i,1);
                }
    }
    
}

function SpawnFood(){
    let x = cellsize/2;
    let y = cellsize/2;
    for (var i = 0; i<Math.floor(canvas.width/cellsize); i++){
        for (var j = 0; j<Math.floor(canvas.height/cellsize); j++){
            if (canMoveTo(x,y,(cellsize-miscarriage*2)/2) && !isThereBigFood(x,y)) {
                new Food(x,y,SmallFood);
            }
            y+=cellsize;
        }
        x+=cellsize;
        y = cellsize/2;
    }
}

class Pacman extends myObject { // нужно имя игрока, если ему хватает очков
    constructor (x,y, size, step, color){
        super(x*cellsize+Math.floor(cellsize/2),y*cellsize+Math.floor(cellsize/2),size,color);
        this.step = step;
        this.direction = "";
        this.next_direction = "";
        this.timer = 0;           //для поворота
        this.angryTimer = 0;     // бонус
        
        this.hideTimer = 0;     // когда умер
        this.lives = StartLives;
        //для отрисовки
        this.start_cadre = 1;
        this.last_cadre = 6;
        this.current_cadre = 1;
        this.to_next_cadre = step;//менять кадр каждые 10 пикселей
        
        this.cadres_hide = 0;
        
        this.Cadres = [];
        
        for (let i = 1; i<=this.last_cadre*4; i++){
            var img = new Image();
            img.src = "pacman\\pac"+i+".png";
            this.Cadres.push(img);
        }
        for (let i = 1; i<=this.last_cadre*4; i++){
            var img = new Image();
            img.src = "pacman\\angrypac"+i+".png";
            this.Cadres.push(img);
        }
        
        this.carde_dir = FORWARD;
        
        MoveObjects.push(this);
        this.UpdateLives();
    }

    move(){
        if (this.angryTimer > 0) {
            let bonus = Math.floor(this.angryTimer/1000);
            this.angryTimer-=speed;
            
            BonusTimer.innerHTML = "Bonus: " +(bonus<10? "0"+bonus : bonus);
        } else BonusTimer.innerHTML ="";
        if (this.hideTimer > 0) {
            this.hideTimer-=speed;
            if (this.hideTimer<=0) this.cadres_hide = 0;
        }
        if (this.timer > 0) this.timer-= speed;
        if (this.timer!=0 && canMoveDirection(this, this.next_direction)){
            this.direction = this.next_direction;
            this.timer = 0;
        }
         
        if((this.to_next_cadre-=this.step)<=0) {this.NextCadre(); this.to_next_cadre = this.step;}
        
        
        if (canMoveDirection(this, this.direction)){
            switch(this.direction){
                case left: {
                    this.x-=this.step; break;
                }
                case right: {
                    this.x+=this.step; break;
                }
                case up: {
                    this.y-=this.step; break;
                }
                case down: {
                    this.y+=this.step; break;
                }
            }
            
         }
        let miss = 0;
        if (this.direction==up ||this.direction==down) {
            miss = (this.x-this.size-miscarriage) % cellsize;
            if (miss<=miscarriage) this.x-=miss;
            else if (miss>=cellsize-miscarriage) this.x+=cellsize-miss;
        }
        if (this.direction==left ||this.direction==right) {
            miss = (this.y-this.size-miscarriage) % cellsize;
            if (miss<=miscarriage) this.y-=miss;
            else if (miss>=cellsize-miscarriage) this.y+=cellsize-miss;
        }
        this.EatOrDie();
     }
    
    EatOrDie(){
        for(var i = 0; i<Foods.length; i++){
            if (this.collision(Foods[i])) {
                Points += Foods[i].type == BigFood ? BigFoodCost : SmallFoodCost;
                if (Foods[i].type == BigFood) this.angryTimer = BonusTime;
                Foods[i].delete();
                if (Foods.length==0) {
                    Win();
                    return;
                }
            }
        }
        for(var i = 0; i<Caspers.length; i++){
            if (this.collision(Caspers[i])) {
                if (Caspers[i].isChild){
                    Points += CasperCost/2;
                    Caspers[i].delete();
                }
                else if (this.angryTimer>0){
                    Points += CasperCost;
                    Caspers[i].delete();
                }
                else if (this.hideTimer<=0){
                    this.lives-=1;
                    if (this.lives<0) {
                        Lose();
                        return;
                    }
                    this.UpdateLives();
                    this.hideTimer = Invulnerable;
                }
            }
        }
        
        
    }
    UpdateLives(){
        Lives.innerHTML="";
        for (var i = 0; i<this.lives; i++)
            Lives.innerHTML+=HTMLlives;
        
    }
    
    
    NextCadre(){
         switch(this.carde_dir){
            case FORWARD: {
                this.current_cadre += 1;
                if (this.current_cadre >= this.last_cadre) this.carde_dir = BACKWARD;
                break;
            }
            case BACKWARD: {
                this.current_cadre -= 1;
                if (this.current_cadre <= this.start_cadre) this.carde_dir = FORWARD;
                break;
            }
        }
    }
    
    
    draw(context){
        if (this.cadres_hide>0) {
            this.cadres_hide--;
            return;
        }
        if (this.hideTimer>0){
            this.cadres_hide--;
            if (this.cadres_hide>=-CadresHideWhenDead) this.cadres_hide=CadresHideWhenDead;
        }
        context.beginPath();
        
        let current_img_index = 
            this.current_cadre+(this.angryTimer>0? this.last_cadre*4-this.start_cadre+1 : 0) -this.start_cadre;
        
        switch(this.direction){
            case down: {
                current_img_index+=this.last_cadre;
                break;
            }
            case left: {
                current_img_index+=this.last_cadre*2;
                break;
            }
            case up: {
                current_img_index+=this.last_cadre*3;
                break;
            }
        }
        
        let img = this.Cadres[current_img_index];
        
        context.drawImage(img, this.x-this.size, this.y-this.size, this.size*2,this.size*2);
        
        context.restore();
        
        context.closePath();
    }
}

class Casper extends myObject {
    constructor(x,y, size, step,color,child){
        super(x*cellsize+Math.floor(cellsize/2),y*cellsize+Math.floor(cellsize/2),size,color);
        this.step = step;
        this.direction = down;
        this.distanceRemains = 0;
        this.cost = CasperCost;
        this.isChild = child==undefined ? true : child;
        if (this.isChild) {
            this.size = size/2;
            this.timer = GrowUpTimer;
        }
        this.sexTimer = 0; 
        var img = new Image();
        img.src = "caspers\\casper.png";
        this.image = img; 
        var img2 = new Image();
        img2.src = "caspers\\child.png";
        this.image_child = img2;
        
        MoveObjects.push(this);
        Caspers.push(this);
    }
    
    changeDirection(){
        var chosenDir = "";
        let av_up = false, av_down = false, av_left = false, av_right = false;
        if (canMoveDirection(this,up, this.isChild ? this.size*2 : undefined)) av_up = true;
        if (canMoveDirection(this,down, this.isChild ? this.size*2 : undefined)) av_down = true;
        if (canMoveDirection(this,left, this.isChild ? this.size*2 : undefined)) av_left = true;
        if (canMoveDirection(this,right, this.isChild ? this.size*2 : undefined)) av_right = true;
                
        let prob = Math.random();
        
        switch(this.direction){
                
            case left: { 
                if (prob<CasperRotationProbability) {
                    if (av_up && av_down) chosenDir = Math.random()<0.5 ? up : down;
                    else chosenDir = av_up ? up : (av_down ? down : "");
                }
                if (chosenDir==""){
                    if (!canMoveDirection(this,left, this.isChild ? this.size*2 : undefined)) this.direction = right;
                }  else this.direction = chosenDir;
                break;
            }
                
            case right: {                
                if (prob<CasperRotationProbability) {
                    if (av_up && av_down) chosenDir = Math.random()<0.5 ? up : down;
                    else chosenDir = av_up ? up : (av_down ? down : "");
                }
                if (chosenDir==""){
                    if (!canMoveDirection(this,right, this.isChild ? this.size*2 : undefined)) this.direction = left;
                }  else this.direction = chosenDir;
                break;
            }
                
            case up: {                
                if (prob<CasperRotationProbability) {
                    if (av_left && av_right) chosenDir = Math.random()<0.5 ? left : right;
                    else chosenDir = av_left ? left : (av_right ? right : "");
                }
                if (chosenDir==""){
                    if (!canMoveDirection(this,up, this.isChild ? this.size*2 : undefined)) this.direction = down;
                }  else this.direction = chosenDir;
                
                break;
            }
                
            case down: {              
                if (prob<CasperRotationProbability) {
                    if (av_left && av_right) chosenDir = Math.random()<0.5 ? left : right;
                    else chosenDir = av_left ? left : (av_right ? right : "");
                }
                if (chosenDir==""){
                    if (!canMoveDirection(this,down, this.isChild ? this.size*2 : undefined)) this.direction = up;
                } else this.direction = chosenDir;
                
                break;
            }
                
        }
        
        return chosenDir;
    }
    
    move(){
        if (sec==0) this.sexTimer = 10000;
        
        if (this.sexTimer>0) {
            this.sexTimer-=speed;
            if (Caspers.length<10&&Caspers.length>1) this.Sex();
        }
        
        if (this.distanceRemains>0) this.distanceRemains-=this.step;
        
        if (this.distanceRemains==0) {
            this.changeDirection();
            this.distanceRemains = cellsize;
        }
        
        switch(this.direction){
            case left: {
                this.x-=this.step; break;
            }
            case right: {
                this.x+=this.step; break;
            }
            case up: {
                this.y-=this.step; break;
            }
            case down: {
                this.y+=this.step; break;
            }
        }
        
        if (this.timer>0) this.timer-=speed;
        else if (this.isChild) this.growUp();
    }
    
    growUp(){ this.isChild = false; this.size *= 2;}
    
    delete(){
        for (var i = 0; i < Caspers.length; i++)
            if (Caspers[i] === this) {
                Caspers.splice(i,1);
            }
       for (var i = 0; i < DrawObjects.length; i++)
            if (DrawObjects[i] === this) {
                DrawObjects.splice(i,1);
            }
    }
    
    
    Sex(){
        //console.log("вызов сЭкс");
        for(var i = 0; i<Caspers.length; i++){
            if (this==Caspers[i]) continue;
            if(this.collision(Caspers[i]) ){
                if (Caspers[i].isChild || this.isChild) continue;
                else {
                    console.log("таки произошел");
                    /*new Casper(Math.floor(this.x/cellsize)*cellsize,
                               Math.floor(this.y/cellsize)*cellsize,
                              (cellsize-miscarriage*2)/2,3,"#dd3030");*/
                    
                    this.sexTimer = 0;
                    Caspers[i].sexTimer = 0;
                    
                    let x = Math.floor(this.x/cellsize);
                    let y = Math.floor(this.y/cellsize);
                    let size = this.size;
                    
                    function selectCellFrom3x3(){
                        if (canPlaceToCell(x,y,size)) return;
                        if (canPlaceToCell(x+1,y,size)) {x+=1; return;}
                        if (canPlaceToCell(x,y+1,size)) {y+=1; return;}
                        if (canPlaceToCell(x+1,y+1,size)) {x+=1;y+=1; return;}
                        if (canPlaceToCell(x-1,y,size)) {x-=1; return;}
                        if (canPlaceToCell(x,y-1,size)) {y-=1; return;}
                        if (canPlaceToCell(x-1,y-1,size)) {x-=1;y-=1; return;}
                    }
                    
                    selectCellFrom3x3();
                    
                        
                        
                    new Casper(x,y,this.size,this.step,this.color);
                }
            }
        }
    }
    
    draw(context){
        context.beginPath();
        
        context.drawImage(
            this.isChild? this.image_child : this.image, this.x-this.size, this.y-this.size, this.size*2,this.size*2);
        
        context.stroke();
        context.closePath();
    }
    
}

function drawquad(x,y,w,color){
        context.beginPath();
        context.rect(x,y,w,w);
        context.fillStyle = color;
        context.fill();
        context.closePath(); 
}

function pixToCell(inpix){
    return Math.floor(inpix/cellsize);
}

function canPlaceToCell(x,y /*in cells*/, size /*in pix*/){
    let can = true;
    can &= canMoveTo(x*cellsize+cellsize/2,y*cellsize+cellsize/2,size);
    return can;
    
}

function canMoveTo(x,y,size){
    //drawquad(x,y,size,"#aaaaaa");
    if (x+size>canvas.width ||
        x-size<0 || 
        y+size>canvas.height || 
        y-size<0 ) 
        return false;
    for (let i=0;i<Walls.length;i++){
        if (Walls[i].contains(x+size,y-size)) return false;
        if (Walls[i].contains(x-size,y-size)) return false;
        if (Walls[i].contains(x+size,y+size)) return false;
        if (Walls[i].contains(x-size,y+size)) return false;
    }
    return true;
}

function isThereBigFood(x,y){
    for (let i = 0; i<BigFoods.length; i++){
        if (Distance(x,y,BigFoods[i].x,BigFoods[i].y)<cellsize) return true; 
        
    }
    return false;
    
}

function canMoveDirection(MovingObject,dir, custom_size){
    let size = custom_size == undefined ? MovingObject.size : custom_size;
    switch(dir){
        case left: {
            return canMoveTo(MovingObject.x - MovingObject.step-miscarriage,MovingObject.y, size);
        }
        case right: {
            return canMoveTo(MovingObject.x + MovingObject.step+miscarriage,MovingObject.y, size);
        }
        case up: {
            return canMoveTo(MovingObject.x, MovingObject.y - MovingObject.step-miscarriage, size);
        }
        case down: {
            return canMoveTo(MovingObject.x, MovingObject.y + MovingObject.step+miscarriage, size);
        }
    }
}

let sec = undefined;
let min = undefined;

function draw() {
    context.clearRect(0,0,canvas.width,canvas.height);
    DrawObjects.forEach(function(item,index,array) {item.draw(context);})
    MoveObjects.forEach(function(item,index,array) {item.move();})
    
    GlobalScore.innerHTML = "Score: "+ Points;
    
    GlobalTime+=speed;
    sec = Math.floor(GlobalTime/1000);
    min = Math.floor(sec/60);
    sec = sec-min*60;
    GlobalTimeWatch.innerHTML = (min<10? "0"+min : min) + ":" + (sec<10? "0"+sec : sec);
    
    /*let dbg1 = doc.getElementById("dbg1");
    let dbg2 = doc.getElementById("dbg2");
    let str = "";
    for (let i = 0; i<Caspers.length; i++)
        str+=Caspers[i].sexTimer+"<br>";
    dbg1.innerHTML=str;
    dbg2.innerHTML=GlobalTime;*/
}

function keyDownHandler(e) {
    if (!gamestarted) return;
    if(e.keyCode == 39) {      /*    R   */
        Pacman1.next_direction = right;
    }
    else if(e.keyCode == 37) { /*    L   */
        Pacman1.next_direction = left;
    }
    else if(e.keyCode == 38) { /*    U   */
        Pacman1.next_direction = up;
    }
    else if(e.keyCode == 40) { /*    D   */
        Pacman1.next_direction = down;
    }
    
    if (canMoveDirection(Pacman1, Pacman1.next_direction))
        Pacman1.direction = Pacman1.next_direction;
    else Pacman1.timer = TimerWait;
    
}

var Pacman1 = undefined;
var PacmanDEMO = undefined;
let timerId = undefined;

PacmanDEMO =new Pacman(9.5,8,80,4,"#c0ff00");
DrawObjects = [];
PacmanDEMO.EatOrDie = function(){};

function angrybigpacman(){
    PacmanDEMO.angryTimer=2*speed;
}

function DEMO(){
    demo_context.clearRect(0,0,canvas.width,canvas.height);
    PacmanDEMO.move();
    PacmanDEMO.draw(demo_context);
}

function Win(){
    setTimeout(function(){
        clearInterval(timerId);
    },250);
    
    gameover.style.color = "forestgreen";
    gameover.innerHTML = "YOU WIN";
    gameover.style.visibility = "visible";
    
    console.log(CanBeRemembered());
    if (CanBeRemembered()){
        RememberBlock.style.visibility="visible";
        NameInput.setAttribute("value","");
    }
    
    setTimeout(ShowButton,500);
}
function Lose(){
    setTimeout(function(){
        clearInterval(timerId);
    },250);
    gameover.style.color = "#e53032";
    gameover.innerHTML = "GAME OVER";
    gameover.style.visibility = "visible";
    
    console.log(CanBeRemembered());
    if (CanBeRemembered()){
        RememberBlock.style.visibility="visible";
        NameInput.setAttribute("value","");
    }
    
    setTimeout(ShowButton,500);
}

function ShowButton(){
    Button.style.visibility = "visible";
    Button.style.opacity = 1;
}


var gameover = doc.getElementById("gameover");

function Init(){
    RememberBlock.style.visibility = "hidden";
    gameover = doc.getElementById("gameover");
    gameover.style.visibility = "hidden";
    Foods = [];
    BigFoods = [];
    Caspers = [];
    Walls = [];
    DrawObjects = [];
    MoveObjects = [];
    var livesRemains = 0;
    if (Pacman1 != undefined){
        livesRemains = Pacman1.lives;
        if (livesRemains<0){
            GlobalTime = 0;
            Points = 0;
        } 
    }
    
    {
new Wall(cellsize*1,cellsize*1, cellsize*1,cellsize*5);
new Wall(cellsize*3,cellsize*1, cellsize*5,cellsize*1);
new Wall(cellsize*8,cellsize*1, cellsize*1,cellsize*2);

new Wall(cellsize*8,cellsize*3, cellsize*2,cellsize*1);

new Wall(cellsize*3,cellsize*3, cellsize*4, cellsize*1);

new Wall(cellsize*3,cellsize*5, cellsize*6, cellsize*1);

new Wall(cellsize*10, 0, cellsize*1, cellsize*4);

new Wall(cellsize*10,cellsize*5, cellsize*1, cellsize*4);
new Wall(0,cellsize*7, cellsize*5, cellsize*1);
new Wall(cellsize*6,cellsize*7, cellsize*5, cellsize*1);

new Wall(cellsize*12,cellsize*7, cellsize*1, cellsize*4);
new Wall(cellsize*12,cellsize*1, cellsize*1, cellsize*5);
new Wall(cellsize*10,cellsize*10, cellsize*1, cellsize*7);

new Wall(cellsize*1,cellsize*9, cellsize*4, cellsize*1);
new Wall(cellsize*1,cellsize*10, cellsize*1, cellsize*5);

new Wall(cellsize*2,cellsize*14, cellsize*3, cellsize*1);
new Wall(cellsize*4,cellsize*11, cellsize*1, cellsize*3);
new Wall(cellsize*3,cellsize*11, cellsize*1, cellsize*1);

new Wall(cellsize*6,cellsize*9, cellsize*3, cellsize*1);
new Wall(cellsize*6,cellsize*10, cellsize*1, cellsize*5);
new Wall(cellsize*7,cellsize*11, cellsize*2, cellsize*1);

new Wall(cellsize*7,cellsize*14, cellsize*2, cellsize*1);
new Wall(cellsize*1,cellsize*16, cellsize*6, cellsize*1);
new Wall(0,cellsize*18, cellsize*3, cellsize*1);

new Wall(cellsize*4,cellsize*18, cellsize*1, cellsize*2);
new Wall(cellsize*6,cellsize*17, cellsize*1, cellsize*2);
new Wall(cellsize*8,cellsize*18, cellsize*5, cellsize*1);

new Wall(cellsize*8,cellsize*16, cellsize*1, cellsize*2);
new Wall(cellsize*11,cellsize*16, cellsize*3, cellsize*1);
new Wall(cellsize*15,cellsize*14, cellsize*1, cellsize*5);


new Wall(cellsize*14,cellsize*1, cellsize*1, cellsize*7);
new Wall(cellsize*15,cellsize*1, cellsize*4, cellsize*1);
new Wall(cellsize*14,cellsize*18, cellsize*1, cellsize*1);

new Wall(cellsize*16,cellsize*7, cellsize*3, cellsize*1);
new Wall(cellsize*16,cellsize*5, cellsize*4, cellsize*1);
new Wall(cellsize*16,cellsize*3, cellsize*3, cellsize*1);
new Wall(cellsize*14,cellsize*9, cellsize*5, cellsize*1);

new Wall(cellsize*12,cellsize*11, cellsize*5, cellsize*1);
new Wall(cellsize*12,cellsize*13, cellsize*1, cellsize*2);
new Wall(cellsize*13,cellsize*14, cellsize*2, cellsize*1);
new Wall(cellsize*14,cellsize*12, cellsize*1, cellsize*1);
new Wall(cellsize*16,cellsize*13, cellsize*1, cellsize*2);
new Wall(cellsize*17,cellsize*13, cellsize*2, cellsize*1);
new Wall(cellsize*18,cellsize*11, cellsize*1, cellsize*8);

new Wall(cellsize*17,cellsize*16, cellsize*1, cellsize*1);
new Wall(cellsize*16,cellsize*18, cellsize*1, cellsize*1);}//Walls
    new Food(0*cellsize+Math.floor(cellsize/2),19*cellsize+Math.floor(cellsize/2),BigFood);
    new Food(19*cellsize+Math.floor(cellsize/2),0*cellsize+Math.floor(cellsize/2),BigFood);
    new Food(9*cellsize+Math.floor(cellsize/2),2*cellsize+Math.floor(cellsize/2),BigFood);
    new Food(17*cellsize+Math.floor(cellsize/2),14*cellsize+Math.floor(cellsize/2),BigFood);
    new Food(7*cellsize+Math.floor(cellsize/2),10*cellsize+Math.floor(cellsize/2),BigFood);
    new Food(0*cellsize+Math.floor(cellsize/2),6*cellsize+Math.floor(cellsize/2),BigFood);
    SpawnFood();
    Pacman1 = new Pacman(10,9,(cellsize-miscarriage*2)/2,4,"#c0ff00");
    if (livesRemains>0) {
        Pacman1.lives = livesRemains;
    }
    {
        new Casper(0,19,(cellsize-miscarriage*2)/2,3,"#dd3030");
        new Casper(19,0,(cellsize-miscarriage*2)/2,3,"#dd3030");
        new Casper(19,19,(cellsize-miscarriage*2)/2,3,"#dd3030");
        new Casper(0,0,(cellsize-miscarriage*2)/2,3,"#dd3030");
    }//Caspers
    setTimeout(draw,100);
}

var gamestarted = false;
function Start(){
    gamestarted = true;
    doc.getElementById("Game").style.visibility = "visible";
    doc.getElementById("StartCanvas").style.visibility = "hidden";
    Button.style.top = "690px";
    Button.innerHTML = "AGAIN";
    Button.style.visibility = "hidden";
    Button.style.opacity = 0;
    
    clearInterval(demo_timer);
    timerId = setInterval(draw, speed);
    /*clearInterval(timerId)*/
}

var demo_timer = setInterval(DEMO, speed);
var demo_scary = undefined;
function scary_on(){
    demo_scary = setInterval(angrybigpacman, speed);
}
function scary_off(){
    clearInterval(demo_scary);
}



document.addEventListener("keydown", keyDownHandler, false);
